﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CrmEntityImage
{
    /// <summary>
    /// Interaction logic for UserUploadWindow.xaml
    /// </summary>
    public partial class UserUploadWindow : Window
    {
        public UserUploadWindow()
        {
            InitializeComponent();
        }

        public byte[] ProfilePicture { get; set; }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".png";
            openFileDialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            if (openFileDialog.ShowDialog() == true)
            {
                txtEditor.Text = openFileDialog.FileName;
            }

            imgPreview.Source = GetThumbnail(openFileDialog.FileName);
        }

        private ImageSource GetThumbnail(string fileName)
        {
            byte[] buffer = File.ReadAllBytes(fileName);
            MemoryStream memoryStream = new MemoryStream(buffer);

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.DecodePixelWidth = 144;
            bitmap.DecodePixelHeight = 144;
            bitmap.StreamSource = memoryStream;
            bitmap.EndInit();
            bitmap.Freeze();

            return bitmap;
        }

        private ImageSource GetThumbnail(byte[] buffer)
        {
            MemoryStream memoryStream = new MemoryStream(buffer);

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.DecodePixelWidth = 144;
            bitmap.DecodePixelHeight = 144;
            bitmap.StreamSource = memoryStream;
            bitmap.EndInit();
            bitmap.Freeze();

            return bitmap;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ProfilePicture.Any())
            {
                imgPreview.Source = GetThumbnail(ProfilePicture);
            }
        }
    }
}
