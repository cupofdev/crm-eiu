﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace CrmEntityImage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public byte[] NoImage { get; set; }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadNoImage()
        {
            NoImage = File.ReadAllBytes("images/User-Profile.png");
        }


        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            var connection = CrmConnection.Parse(txtCrmConnectionString.Text);
            var service = new OrganizationService(connection);
            var context = new CrmOrganizationServiceContext(connection);
            string fetchXml = @"<fetch version=""1.0"" output-format=""xml-platform"" mapping=""logical"" distinct=""false"">
                                <entity name=""contact"">
                                <attribute name=""fullname"" />
                                <attribute name=""entityimage"" />
                                <attribute name=""contactid"" />
                                <order attribute=""fullname"" descending=""false"" />
                                <filter type=""and"">
                                <condition attribute=""statecode"" operator=""eq"" value=""0"" />
                                </filter>
                                 </entity>
                                 </fetch>";
            EntityCollection binaryImageResults = context.RetrieveMultiple(new FetchExpression(fetchXml));
            List<Contact> contacts = new List<Contact>();
            foreach (Entity record in binaryImageResults.Entities)
            {
                String recordName = record["fullname"] as String;
                Contact contact = new Contact();
                contact.FullName = recordName;
                contact.Id = Guid.Parse(record["contactid"].ToString());

                String downloadedFileName = String.Format("Downloaded_{0}", recordName);
                if (record.Attributes.Contains("entityimage"))
                {
                    byte[] imageBytes = record["entityimage"] as byte[];
                    contact.Image = imageBytes;
                    //var fs = new BinaryWriter(new FileStream(downloadedFileName, FileMode.Append, FileAccess.Write));
                    //fs.Write(imageBytes);
                    //fs.Close();
                }
                else
                {
                    contact.Image = NoImage;
                }
                contacts.Add(contact);
            }

            dtImages.ItemsSource = contacts;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadNoImage();
        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            UserUploadWindow upw = new UserUploadWindow();

            System.Windows.Controls.Button button = sender as System.Windows.Controls.Button;
            var context = button.DataContext as Contact;
            upw.ProfilePicture = context.Image;
            upw.Show();
        }
    }
}
